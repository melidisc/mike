import numpy as np
import gensim
from tools import shuffle, minibatch, contextwin, loadFile
from elman import model


import pdb

if __name__=="__main__":

    net_hparams = {'fold':3, # 5 folds 0,1,2,3,4
         'lr':0.0627142536696559,
         'verbose':1,
         'decay':False, # decay on the learning rate if improvement stops
         'win':3, # number of words in the context window
         'bs':9, # number of backprop through time steps
         'nhidden':100, # number of hidden units
         'seed':345,
         'emb_dimension':200, # dimension of word embedding
         'nepochs':2}#50}

     raw_dataset = loadFile("all_data_saved.pickle")

     sentences = []
     for sentence in dataset:
         new_sentence = []
         for word in sentence:
             new_sentence.append(word[0])
         sentences.append(new_sentence)


     word2vec_model = gensim.models.Word2Vec.load("skipgram_all_data")

     #embedding dimension
     emb_dimension = word2vec_model["one"].shape[0]

     #the network
     rnn = model(    nh = net_hparams['nhidden'],#number of hidden layers
                     nc = emb_dimension,#number of output units
                     ne = len(word2vec_model.vocab)-1,
                     de = net_hparams['emb_dimension'],
                     cs = net_hparams['win'] )

     for e in xrange(s['nepochs']):
         # shuffle
         shuffle(sentences, s['seed'])
         net_hparams['ce'] = e
         tic = time.time()
         for i in xrange(len(sentences)):

             cwords = contextwin(sentences[i], s['win'])

             words  = map(lambda x: numpy.asarray(x).astype('int32'),\
                          minibatch(cwords, s['bs']))

             labels = train_y[i]

             for word_batch , label_last_word in zip(words, labels):
                 word_batch2 = word_batch

                 # print "Word batch being \n",word_batch2, [idx2word[x] for x in word_batch2.tolist()[0]], "\n label last words being \n", idx2word[label_last_word], "\n vocsize \n", vocsize
                 rnn.train(word_batch, label_last_word, s['clr'])
                 rnn.normalize()
             if s['verbose']:
                 print '[learning] epoch %i >> %2.2f%%'%(e,(i+1)*100./nsentences),'completed in %.2f (sec) <<\r'%(time.time()-tic),
                 sys.stdout.flush()
