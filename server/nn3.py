import numpy as np
import gensim
from tools import shuffle, minibatch, contextwin, loadFile
from elman2 import RNN_BPTT as rnn
import theano

import pdb

# network
class ANN():
    def __init__(self,):
        # global network
        print "Loading model..."
        self.word2vec_model = gensim.models.Word2Vec.load(
            "skipgram_all_data")
        print "Done."
        print "Loading dataset..."
        self.raw_dataset = loadFile("all_data_saved.pickle")
        print "Done."

        print "Loading sentences..."
        self.sentences = []
        for sentence in self.raw_dataset:
            new_sentence = []
            for word in sentence:
                new_sentence.append(self.word2vec_model[word[0]])
            self.sentences.append(new_sentence)
        print "Done."

        #embedding dimension
        emb_dimension = self.word2vec_model["one"].shape[0]

        #net parameters
        self.n_hp = {
            "in_d":emb_dimension,
            "h_d":emb_dimension*3,
            "out_d":emb_dimension,
            "lr":0.00627142536696559,
            "epochs":100,
            "seed":345,
            "bs":9,
            "c_win":7
            }
        print "Initalising the network..."
        #init the network
        self.network = rnn(
            self.n_hp["h_d"],
            self.n_hp["in_d"],
            self.n_hp["out_d"]
            )
        print "Done."
        print "Loading weights..."
        self.network._load("trained/nn3_save")
        print "Done."
        self.trainer = self.network._net_trainer_accumulator()
        self.stepper = self.network._net_step()
    
    def train(self,):
        '''
        for i in xrange(len(self.sentences)):
            #prepare the minibatch
            words  = map(lambda x: np.asarray(x).astype(theano.config.floatX),\
                      minibatch(sentences[i], n_hp['bs']))

            #slide for prediction
            target = self.sentences[i][1:]
            #append the end of the sentence
            target.append(np.zeros(n_hp['in_d']))
            #prepare the target
            targets = map(lambda x: np.asarray(x).astype(theano.config.floatX),\
                      minibatch(target, n_hp['bs']))
        '''

        for e in xrange(self.n_hp["epochs"]):

            shuffle(self.sentences, self.n_hp['seed'])
            GE = .0
            for i in xrange(len(self.sentences)):

                inp = self.sentences[i]
                outp = self.sentences[i][1:]
                outp.append(np.zeros(self.n_hp['in_d']))
                Error = 0.
                # import pdb; pdb.set_trace()
                if len(inp)==0 or len(outp)==0:
                    continue

                error, output = self.trainer(inp,outp,self.n_hp["lr"])
                Error += error

                print "Sentence ", i, " error ", Error/self.n_hp['bs']
                print "The sentence is :"
                # for word in output:
                #     out = self.word2vec_model.most_similar(positive=[word],topn=1)
                #     print " ",out[0][0],
                print " "
                GE +=Error
                if i%100==0:
                    self.network._save("nn3_save")
            print "Epoch ",e," error ", GE/len(self.sentences)
            if e %10:
                self.n_hp["lr"] *=.9
                self.network._save("nn3_save")
            #import pdb; pdb.set_trace()
    def reverse(self, words):
        return [x for x in reversed(words)]
    def step(self,words):
        go = True
        rep_counter = 0
        while go:
            vector = []
            for word in self.reverse(words):
                #find its vector
                try:
                    v = self.word2vec_model[word]
                except:
                    print "Word not found"
                else:
                    vector.append(
                    self.word2vec_model[word])
            
            if vector !=[]:
                #put it in the network
                h, y = self.stepper(vector)
            else:
                return words, [h,y]
            #take output
            word_out = self.word2vec_model.most_similar(positive=[y],topn=1)[0][0]
            # import pdb;pdb.set_trace()
            print word_out
            if word_out != words[-1]:
                # words = self.reverse(words)
                words.append(word_out)
            else:
                rep_counter +=1
                word_out = self.word2vec_model.most_similar(positive=[y],topn=10)[rep_counter][0]
                print word_out
                if word_out != words[-1]:
                    # words = self.reverse(words)
                    words.append(word_out)
                
            
            mse = np.sum((y)**2)/y.shape[0]
            if len(words)>20 or (rep_counter>5):
                return words, [h,y]

        
if __name__=="__main__":

    # try:
    a = ANN()
    word_out, [h,y] = a.step("rediscover london town".split(" "))
    print word_out
    import pdb;pdb.set_trace()
    