#server
from gevent import monkey
monkey.patch_all()


import flask
from flask import Flask, render_template
from flask.ext.socketio import SocketIO, emit

#engine

from nn3 import ANN

#os
from datetime import datetime
import os, sys
import numpy as np
from time import sleep,time
from threading import Thread
import subprocess
import re


class Serverakla():
    def __init__(self,folderName=None):
        self.app = Flask(__name__)
        self.app.config.from_pyfile('config.py')
        self.ws = SocketIO(self.app)
        self.ann = ANN()
        self.regex = re.compile('[%s]' % re.escape('!"$%&\'()*+,-./:;<=>?[\\]^_`{|}~'))

        @self.app.route('/')
        def main():
            return render_template('index.html')

        self.ws.on(
            'tweet', namespace='/back')(
            self.tweet)
        self.ws.on(
            'word', namespace='/back')(
            self.word)
        self.ws.on(
            'odd_word_list', namespace='/back')(
            self.odd_word_list)

    def start(self):
        port = int(os.environ.get("PORT", 3001))
        self.ws.run(self.app, host='0.0.0.0', port=port)
    
    def tweet(self,data):
        
        t = data["tweet"]
        print "Tweet received ",t
        t = self.sanitiseTweet(t)
        print "Tweet sanitised ",t
        word_out =""
        if t == []:
            word_out = "Sorry, try again" 
        else:
            out, [h,y] = self.ann.step(t)
            for w in out:
                word_out += w+" "
        print "Network output ", word_out
        
        data = {
            "msg":word_out
        }
        emit(
            'tweet',
            data,
            namespace='/back',
            broadcast=False
            )
        return True
    def word(self,data):
        t = data["word"]
        print "Word received ",t
        t = self.sanitiseTweet(t)[0]
        print "Word sanitised ",t
        word_list = []
        if t != []:
            try:
                word_list = self.ann.word2vec_model.most_similar(t, topn=5)
            except:
                word_list=[("Sorry","try again")]
        else:
            word_list=[("Sorry","try again")]
        data = {
            "msg":word_list
        }
        emit(
            'similar_words_list',
            data,
            namespace='/back',
            broadcast=False
            )
        return True
    def odd_word_list(self,data):
        t = data["words"]
        print "Word received ",t
        t = self.sanitiseTweet(t)
        print "Word sanitised ",t
        
        if t != []:
            try:
                odd_one = self.ann.word2vec_model.doesnt_match(t)
            except:
                import pdb;pdb.set_trace() 
                odd_one = "Sorry, try again"
        else:
            odd_one= "Sorry, try again"

        data = {
            "msg":odd_one
        }
        emit(
            'odd_words_list',
            data,
            namespace='/back',
            broadcast=False
            )
        return True
    def sanitiseTweet(self,tweet):
        
        def test_re(s):
            return self.regex.sub(' ', s)

        words = tweet.lower().split(" ")

        new_words = []
        for word in words:
            if len(word)<1:
                continue
            if ("http" in word):
                continue
            if (u'\ufffd' in word):
                continue
            word = test_re(word).lower().split(" ")
            # print word
            if len(word)>1:
                true_words = []
                for w in word:
                    if (w=="") or (w ==" "):
                        continue
                    true_words.append(w)
                new_words.extend(true_words)
            else:
                new_words.append(word[0])
        return new_words

if __name__ == '__main__':
    print "Starting server"
    server = Serverakla()

    server.start()

    sys.exit(1)
